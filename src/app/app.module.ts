import { AppComponent } from './components/app';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { NgModule } from '@angular/core';
import { DictionaryComponent } from './components/dictionary.component';
import { RouterModule } from '@angular/router';
import { StoreModule } from 'ng-state';
import { DictionariesComponent } from './components/dictionaries.component';
import { initialState } from './initial-state';
import { routes } from './routes';
import { ValidationService } from './services/validation.service';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(routes, { useHash: true }),
    StoreModule.provideStore(initialState),
  ],
  declarations: [
    AppComponent,
    DictionariesComponent,
    DictionaryComponent
  ],
  providers: [
    ValidationService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {
}