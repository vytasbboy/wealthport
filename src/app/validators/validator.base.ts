import * as Immutable from 'immutable';

import { DictionaryValue } from '../actions/dictionary.model';

export abstract class Validator {
    abstract problem: Problem;
    abstract validate(targetValue: DictionaryValue, currentValues: Immutable.Map<string, string>): ValidationResult;
}

export class ValidationResult {
    isValid: boolean;
    constructor(public matches: any, public problem: Problem) {
        this.isValid = !matches || matches.size === 0;
    }
}

export const enum Problem {
    None,
    Low,
    Normal,
    High,
    VyryHigh,
}