import * as Immutable from 'immutable';

import { Problem, ValidationResult, Validator } from './validator.base';

import { DictionaryValue } from '../actions/dictionary.model';

export class ChainsValidator extends Validator {
    problem = Problem.VyryHigh;

    validate(targetValue: DictionaryValue, currentValues: Immutable.Map<string, string>): ValidationResult {
        const matches = currentValues.filter((value: any) => {
            return !this.areDomainsEqual(targetValue.domain, value.get('domain'))
                && this.isRangeEqualToDomain(targetValue.range, value.get('domain'))
                && !this.areRangesEqual(targetValue.range, value.get('range'));
        });

        return new ValidationResult(matches, this.problem);
    }

    private areDomainsEqual(domainOne: string, domainTwo: string) {
        return domainOne === domainTwo;
    }

    private isRangeEqualToDomain(range: string, domain: string) {
        return range === domain;
    }

    private areRangesEqual(rangeOne: string, rangeTwo: string) {
        return rangeOne === rangeTwo;
    }
}