import * as Immutable from 'immutable';

import { Problem, ValidationResult, Validator } from './validator.base';

import { DictionaryValue } from '../actions/dictionary.model';

export class CyclesValidator extends Validator {
    problem = Problem.High;

    validate(targetValue: DictionaryValue, currentValues: Immutable.Map<string, string>): ValidationResult {
        const matches = currentValues.filter((value: any) => {
            return targetValue.domain === value.get('range') && targetValue.range === value.get('domain');
        });

        return new ValidationResult(matches, this.problem);
    }
}