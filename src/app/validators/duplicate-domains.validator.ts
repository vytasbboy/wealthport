import * as Immutable from 'immutable';

import { Problem, ValidationResult, Validator } from './validator.base';

import { DictionaryValue } from '../actions/dictionary.model';

export class DuplicateDomainsValidator extends Validator {
    problem = Problem.Normal;

    validate(targetValue: DictionaryValue, currentValues: Immutable.Map<string, string>): ValidationResult {
        const matches = currentValues.filter((value: any) => {
            return targetValue.domain === value.get('domain');
        });

        return new ValidationResult(matches, this.problem);
    }
}