import * as Immutable from 'immutable';

import { Problem, ValidationResult, Validator } from './validator.base';

import { DictionaryValue } from '../actions/dictionary.model';

export class DuplicateDomainsAndRangesValidator extends Validator {
    problem = Problem.Low;

    validate(targetValue: DictionaryValue, currentValues: Immutable.Map<string, string>): ValidationResult {
        const matches = currentValues.filter((value: any) => {
            return targetValue.domain === value.get('domain') && targetValue.range === value.get('range');
        });

       return new ValidationResult(matches, this.problem);
    }
}