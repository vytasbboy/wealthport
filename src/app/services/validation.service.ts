import * as Immutable from 'immutable';

import { Problem, Validator } from '../validators/validator.base';

import { ChainsValidator } from '../validators/chains.validator';
import { CyclesValidator } from '../validators/cycles.validator';
import { DictionaryValue } from '../actions/dictionary.model';
import { DuplicateDomainsAndRangesValidator } from '../validators/duplicate-domains-and-ranges.validator';
import { DuplicateDomainsValidator } from '../validators/duplicate-domains.validator';
import { Injectable } from '@angular/core';
import { ValidationResult } from './../validators/validator.base';

@Injectable()
export class ValidationService {
    private validators: Validator[] = [];

    constructor() {
        this.validators.push(new DuplicateDomainsAndRangesValidator());
        this.validators.push(new DuplicateDomainsValidator());
        this.validators.push(new CyclesValidator());
        this.validators.push(new ChainsValidator());
    }

    validate(targetValue: DictionaryValue, currentValues: Immutable.Map<string, string>) {

        if (!currentValues) {
            return new ValidationResult(null, Problem.None);
        }

        let results: ValidationResult;
        for (let i = 0; i < this.validators.length; i++) {
            results = this.validators[i].validate(targetValue, currentValues);
            if (!results.isValid) {
                break;
            }
        }

        return results;
    }
}