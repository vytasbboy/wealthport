import { DictionaryModel } from './actions/dictionary.model';
import { Problem } from './validators/validator.base';

export function initialState() {
  return {
    dictionaries: <DictionaryModel[]>[
      <DictionaryModel>{
        id: 1,
        title: 'Test 1',
        description: 'Test 1 description',
        values: [
          { domain: 'Stonegrey', range: 'Dark Grey', problem: Problem.None },
          { domain: 'Stonegrey', range: 'Dark Grey', problem: Problem.None },
          { domain: 'Midnight Blue', range: 'Dark Blue', problem: Problem.None },
          { domain: 'Black', range: 'Silver', problem: Problem.None }
        ]
      }
    ]
  };
}