import { Routes } from '@angular/router';
import { DictionariesComponent } from './components/dictionaries.component';
import { DictionaryComponent } from './components/dictionary.component';

export const routes: Routes = [
  {
    path: '', redirectTo: 'dictionaries', pathMatch: 'full'
  },
  {
    path: 'dictionaries',
    component: DictionariesComponent,
  },
  {
    path: 'dictionaries/:id',
    component: DictionaryComponent
  }
];
