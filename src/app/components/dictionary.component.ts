import { ActivatedRoute, Params, Router } from '@angular/router';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ComponentState, HasStateActions } from 'ng-state';

import { DictionaryStateActions } from './../actions/dictionary.actions';
import { DictionaryValue } from './../actions/dictionary.model';
import { Problem } from '../validators/validator.base';
import { ValidationService } from '../services/validation.service';

@ComponentState(DictionaryStateActions)
@Component({
  selector: 'dictionary',
  styles: [
    'table { max-width: 550px; }',
    'form { max-width: 550px; }',
    '.alert { max-width: 550px; }',
    '.col-delete { width: 27px; }',
    '.conflicts { text-align: center; position: relative; }',
    '.conflict { position: absolute; width: 22px; height: 22px; top: 0; bottom: 0; left: 0; right: 0; margin: auto; box-shadow: 0px 0px 6px #888888; border-radius: 11px; border: 1px solid #FCFCFC; }',
    'nav a { padding: 0; margin-bottom: 20px; }',
    '.legend { margin-top: 20px; display: flex; flex-wrap: wrap; }',
    '.legend .problem-holder { display: flex; margin-right: 15px; }',
    '.legend .problem { width: 22px; height: 22px; box-shadow: 0px 0px 6px #888888; border-radius: 11px; border: 1px solid #FCFCFC; }',
    '.legend .text { display: block; padding-left: 8px; }',
    '.separator { max-width: 620px; height: 1px; background-color: #CCC; margin-bottom: 20px; margin-top: 60px; }'
  ],
  templateUrl: './dictionary.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DictionaryComponent extends HasStateActions<DictionaryStateActions> implements OnInit {
  isValid = true;

  model = {
    domain: '',
    range: ''
  };

  constructor(private route: ActivatedRoute, private router: Router, private validationService: ValidationService) {
    super();
    this.route.params.subscribe((params: Params) => {
      (<any>this).stateIndex = params.id;
    });
  }

  ngOnInit() {
    this.actions.resetSeverities();

    if (!this.actions.values) {
      this.router.navigate(['']);
    }
  }

  deleteItem(index: number) {
    this.actions.deleteValue(index);
  }

  addItem(e: Event) {

    if (!this.isInputValid()) {
      this.isValid = false;
      return;
    }

    this.isValid = true;

    this.actions.resetSeverities();

    const itemToAdd = { domain: this.model.domain, range: this.model.range, problem: Problem.None } as DictionaryValue;
    const validationResults = this.validationService.validate(itemToAdd, this.actions.values);

    if (validationResults.isValid) {
      this.actions.addValue(itemToAdd);
      this.resetFormValues();
    } else {
      this.actions.setSeverities(validationResults.matches, validationResults.problem);
    }
  }

  trackById(index: number) {
    return index;
  }

  private isInputValid() {
    return this.model.domain !== '' && this.model.range !== '';
  }

  private resetFormValues() {
    this.model.domain = '';
    this.model.range = '';
  }
}