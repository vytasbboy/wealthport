import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'wealthport-app',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styles: [
    '.container {margin-top: 30px}',
  ],
  template: `
      <div class="container">
        <router-outlet></router-outlet>
      </div>

      <state-history></state-history>
  `
})
export class AppComponent {
}