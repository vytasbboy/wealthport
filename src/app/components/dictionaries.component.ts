import { ComponentState, HasStateActions } from 'ng-state';

import { Component } from '@angular/core';
import { DictionariesStateActions } from '../actions/dictionaries.actions';
import { DictionaryModel } from '../actions/dictionary.model';

@ComponentState(DictionariesStateActions)
@Component({
  selector: 'dictionaries',
  styles: [
    '.card-deck .card {max-width: 300px; min-width: 300px; margin-left:0; margin-right: 30px; margin-bottom: 30px; }',
    'a { color: white };',
    ' a:hover { color: #5bc0de };',
    '.href-block {justify-content: space-between; max-height: 64px; }',
    '.href-block button { color: white; text-shadow: none; opacity: 1; outline: none; }',
    '.href-block button:hover { color: #5bc0de }',
    'form { margin-bottom: 30px; }',
    '.alert { max-width: 550px; }'
  ],
  template: `
      <h4>Add Dictionary</h4>
      <form class="form-inline">
        <input type="text" title="title" [(ngModel)]="model.title" name="title" class="form-control mb-2 mr-sm-2 mb-sm-0" id="title" placeholder="Title" />
        <input type="text" name="description" [(ngModel)]="model.description" class="form-control mb-2 mr-sm-2 mb-sm-0" id="descritpion" placeholder="Description" />
        <button type="button" class="btn btn-primary" (click)="addDictionary()">Submit</button>
      </form>
      <div *ngIf="!isValid">
        <p>
          <div class="alert alert-danger" role="alert">Both fields are required</div>
        <p>
      </div>
      <h4>Dictionaries</h4>
      <div class="card-deck">
          <div class="card" style="width: 20rem;" *ngFor="let dictionary of actions.dictionaries | async; let i = index; trackBy: trackById;" >
            <div class="card-block">
              <h4 class="card-title">{{ dictionary.title }}</h4>
              <p class="card-text">{{ dictionary.description }}</p>
            </div>
            <div class="card-block card-primary href-block">
              <a href="#" [routerLink]="['/dictionaries', i]" class="card-link">Go To Values</a>
              <button type="button" class="close" aria-label="Close" (click)="deleteDictionary(i)">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="card-footer">
              <small class="text-muted"><strong>Items in dictionary:</strong> {{ dictionary.itemsCount }}</small>
            </div>
        </div>
     </div>
  `
})
export class DictionariesComponent extends HasStateActions<DictionariesStateActions> {
  isValid = true;
  model = {
    title: '',
    description: ''
  };

  trackById(item: DictionaryModel) {
    return item.id;
  }

  addDictionary(e: Event) {
    if (!this.isInputValid()) {
      this.isValid = false;
      return;
    }

    this.isValid = true;
    this.actions.addDictionary(new DictionaryModel(this.model.title, this.model.description));
    this.model.title = '';
    this.model.description = '';
  }

  deleteDictionary(index: number) {
    this.actions.deleteDictionary(index);
  }

  private isInputValid() {
    return this.model.title !== '' && this.model.description !== '';
  }
}