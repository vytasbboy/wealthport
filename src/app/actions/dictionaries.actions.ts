import * as Immutable from 'immutable';

import { HasStore, Helpers, InjectStore } from 'ng-state';

import { DictionaryModel } from '../actions/dictionary.model';

@InjectStore('dictionaries')
export class DictionariesStateActions extends HasStore<Immutable.List<any>> {
    get dictionaries() {
        return this.store.map(state =>
            state.map(item => {
                return {
                    id: item.get('id'),
                    title: item.get('title'),
                    description: item.get('description'),
                    itemsCount: item.get('values').size
                };
            })
        );
    }

    deleteDictionary(index: number) {
        this.store.update((state: any) => {
            state.delete(index);
        }, false);
    }

    addDictionary(dictionary: DictionaryModel) {
        this.store.update(state => {
            Helpers.overrideContructor(dictionary);
            state.push(Immutable.fromJS(dictionary));
        }, false);
    }
}