import * as Immutable from 'immutable';

import { HasStore, InjectStore } from 'ng-state';

import { DictionaryValue } from './dictionary.model';
import { Problem } from '../validators/validator.base';

@InjectStore('dictionaries/${stateIndex}')
export class DictionaryStateActions extends HasStore<Immutable.Map<any, any>> {

    get title() {
        return this.store.map(state => {
            return state.get('title');
        });
    }

    get description() {
        return this.store.map(state => {
            return state.get('description');
        });
    }

    addValue(item: DictionaryValue) {
        this.store.update(state => {
            state.get('values').push(Immutable.fromJS(item));
        }, false);
    }

    deleteValue(index: number) {
        this.store.update((state: Immutable.Map<any, any>) => {
            state.get('values').delete(index);
        }, false);
    }

    get values() {
        return this.state
            ? this.state.get('values')
            : null;
    }

    get valuesMapped() {
        return this.store.map(state =>
            state.get('values').map(item => {
                return {
                    domain: item.get('domain'),
                    range: item.get('range'),
                    problem: item.get('problem')
                };
            })
        );
    }

    resetSeverities() {
        this.store.update((state: any) => {
            state.updateIn(['values'], values => {
                return values.map(item => item.set('problem', ''));
            });
        });
    }

    setSeverities(itemsToReset: Immutable.List<any>, problem: Problem) {
        this.store.update((state: Immutable.Map<any, any>) => {
            state.get('values').update(values => values.map(item => {
                return itemsToReset.find(targetValue => item === targetValue)
                    ? item.set('problem', problem)
                    : item;
            }));
        }, false);
    }
}